class Game {
  constructor() {
    if (this.constructor === Game) {
      throw new Error("Cannot instantiate from Abstract Class");
    }
  }

  gameWin() {
    hasil.innerHTML = "PLAYER 1 WIN";
    bgversus.style.backgroundColor = "#4C9654";
    bgversus.style.transform = "rotate(-28.87deg)";
    bgversus.style.borderRadius = "10px";
  }

  gameLose() {
    hasil.innerHTML = "COM WIN";
    bgversus.style.backgroundColor = "#4C9654";
    bgversus.style.transform = "rotate(-28.87deg)";
    bgversus.style.borderRadius = "10px";
  }

  gameDraw() {
    hasil.innerHTML = "DRAW";
    bgversus.style.backgroundColor = "#035B0C";
    bgversus.style.transform = "rotate(-28.87deg)";
    bgversus.style.borderRadius = "10px";
  }
}

class Player extends Game {
  constructor(props) {
    super();
    this._playerName = Player.name;
  }

  gameWin() {
    super.gameWin();
    hasil.style.fontSize = "38px";
    hasil.style.color = "white";
    hasil.style.fontWeight = "700";
    hasil.style.letterSpacing = "0.1 rem";
  }

  gameLose() {
    super.gameLose();
    hasil.style.fontSize = "38px";
    hasil.style.color = "white";
    hasil.style.fontWeight = "700";
    hasil.style.letterSpacing = "0.1 rem";
  }

  gameDraw() {
    super.gameDraw();
    hasil.style.fontSize = "38px";
    hasil.style.color = "white";
    hasil.style.fontWeight = "700";
    hasil.style.letterSpacing = "0.1 rem";
  }
}

const bgversus = document.getElementById("backgroundversus");
const hasil = document.getElementById("versus");

const bBatu = document.getElementById("backgroundbatu");
const bKertas = document.getElementById("backgroundkertas");
const bGunting = document.getElementById("backgroundgunting");

const cBatu = document.getElementById("comBatu");
const cKertas = document.getElementById("comKertas");
const cGunting = document.getElementById("comGunting");

const player = new Player();

function suit(pilihan) {
  document.getElementById("playerBatu").disabled = true;
  document.getElementById("playerKertas").disabled = true;
  document.getElementById("playerGunting").disabled = true;

  var comMath = Math.floor(Math.random() * 3) + 1;
  var com = "";

  if (pilihan == "batu") {
    bBatu.style.backgroundColor = "#C4C4C4";
    bBatu.style.borderRadius = "30px";
  } else if (pilihan == "kertas") {
    bKertas.style.backgroundColor = "#C4C4C4";
    bKertas.style.borderRadius = "30px";
  } else if (pilihan == "gunting") {
    bGunting.style.backgroundColor = "#C4C4C4";
    bGunting.style.borderRadius = "30px";
  }

  switch (comMath) {
    case 1:
      cBatu.style.backgroundColor = "#C4C4C4";
      cBatu.style.borderRadius = "30px";
      com = "Batu";
      if (pilihan == "kertas") {
        player.gameWin();
      } else if (pilihan == "gunting") {
        player.gameLose();
      } else {
        player.gameDraw();
      }
      break;

    case 2:
      cKertas.style.backgroundColor = "#C4C4C4";
      cKertas.style.borderRadius = "30px";
      com = "Kertas";
      if (pilihan == "batu") {
        player.gameLose();
      } else if (pilihan == "gunting") {
        player.gameWin();
      } else {
        player.gameDraw();
      }
      break;

    case 3:
      cGunting.style.backgroundColor = "#C4C4C4";
      cGunting.style.borderRadius = "30px";
      com = "Gunting";
      if (pilihan == "kertas") {
        player.gameLose();
      } else if (pilihan == "batu") {
        player.gameWin();
      } else {
        player.gameDraw();
      }
      break;
  }

  console.log(pilihan);
}

function reset() {
  document.getElementById("playerBatu").disabled = false;
  document.getElementById("playerKertas").disabled = false;
  document.getElementById("playerGunting").disabled = false;

  bBatu.style.backgroundColor = "transparent";
  bKertas.style.backgroundColor = "transparent";
  bGunting.style.backgroundColor = "transparent";

  cBatu.style.backgroundColor = "transparent";
  cKertas.style.backgroundColor = "transparent";
  cGunting.style.backgroundColor = "transparent";

  bgversus.removeAttribute("style");
  hasil.style.color = "#bd0000";
  hasil.style.fontSize = "144px";
  hasil.innerHTML = "VS";
}
